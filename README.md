[![](https://images.microbadger.com/badges/image/cogentwebs/base.svg)](https://microbadger.com/images/cogentwebs/base "Get your own image badge on microbadger.com")

# Docker: alpine-glibc with  s6 & sock overlay

Docker alpine & s6-overlay image built on alpine-glibc Linux micro OS with the final image size of ~25 MB vs. 100+ MB compared to the Ubuntu / Debian equivalent. It's intended to be used as a base image for building final app images ready for deployment.

# Running

Pull or build the image yourself and run it. Before you do that you'll need to configure s6-overlaygggggg and your services ready for "dockerization".

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)